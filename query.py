import rdflib

from helper import *

__author__ = 'pablo'

g = rdflib.Graph()


def executeQuery(query, loop, verbose):
    """
    Executes the provided query and handles errors
    :param query: Query to execute
    :param loop: bool that holds if we need to abort when error
    :param verbose: Level of verbosity
    """

    if verbose:
        print_yellow('Query: ' + query)
    try:
        execution = g.query(query)
        for result in execution:
            print result
        print '(' + str(len(execution)) + ' row(s) affected.)'
    except:
        print_red('The query could not be completed successfully. Please review it and run it again.')
        if not loop:
            sys.exit(1)

def consoleMode():
    """
    User input handling
    :return queryInput: string containing all user input
    """
    doubleEnter = 0
    queryInput = ''
    while doubleEnter < 1:
        userInput = raw_input('> ')
        if len(userInput) == 0:
            doubleEnter += 1
        queryInput += userInput + ' '
    return queryInput

def query(args):

    if args.input is None:
        filename = getFilename(args.input)
    else:
        filename = args.input

    if args.verbose:
        print_yellow('Input filename: ' + filename)

    try:
        inputFile = open(filename, 'r')
        g.parse(inputFile)
    except:
        print_red(
            ('It was not possible to read the image and create the graph. '
             'Please make sure that you provide an valid existing rdf.'))
        sys.exit(1)

    if args.console:
        if args.verbose:
            print_green('Console Mode, empty line to execute.')
        try:
            while True:
                queryInput = consoleMode()
                executeQuery(queryInput, True, args.verbose)
        except KeyboardInterrupt:
            if args.verbose:
                print_green('Exiting...')
            sys.exit(1)

    else:

        executeQuery(args.execute, False, args.verbose)

        if args.verbose:
            print_green('Query execution completed successfully. Exiting...')


if __name__ == '__main__':

    printHeader()
    arguments = parseArguments(2)
    query(arguments)