# Object Recognition CLI
## Descripción
CLI que utiliza la búsqueda inversa de imágenes de Google para etiquetar una imagen. Basada en los suiguientes comandos cURL:

* Upload image:

    ```curl -v --form "encoded_image=@~/Desktop/tests/large-screen.png" http://images.google.com/searchbyimage/upload -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.149 Safari/537.36'```

* Link to image by URI: 

    ```curl 'http://images.google.com/searchbyimage?image_url=http%3A%2F%2Fmedicalantiques.com%2Fmedical%2Fmedimage%2Fb1.jpg&image_content=&filename='```
    
## Dependencias

Se pueden instalar mediante el comando `pip install [nombre]`

* [poster](http://atlee.ca/software/poster/)
* [BeautifulSoup](http://www.crummy.com/software/BeautifulSoup/)
* [termcolor](https://pypi.python.org/pypi/termcolor/1.1.0)
* [findtools](https://github.com/ewiger/findtools)
* [rdflib](https://github.com/RDFLib/rdflib)

## Uso
#### Object Recognition Main
***

    usage: main.py [-h] [--verbose] [--path PATH] [--recursive] [--output OUTPUT]
    Will output a best match label for a given image.
    optional arguments:
      -h, --help            show this help message and exit
      --verbose, -V         Increase output verbosity.
      --path PATH, -p PATH  Path to the image or folder to tag.
      --recursive, -R       Recursively tags all images encountered. Remember to
                            provide a path to a folder when using this argument.
      --output OUTPUT, -o OUTPUT
                            Output filename. Defaults to output.rdf

#### Object Recognition Query
***

    usage: query.py [-h] [--verbose] [--input INPUT]
                [--execute EXECUTE | --console]
    Query a RDF.
    optional arguments:
      -h, --help            show this help message and exit
      --verbose, -V         Increase output verbosity.
      --input INPUT, -i INPUT
                            Input filename. Defaults to output.rdf.
      --execute EXECUTE, -x EXECUTE
                            Query to execute.
      --console, -c         Console mode is turned on.