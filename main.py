import urllib2

from helper import *
from constants import URL, USERAGENT
from rdflib.namespace import RDF, FOAF
from BeautifulSoup import BeautifulSoup
from rdflib import BNode, Literal, Graph
from poster.encode import multipart_encode
from poster.streaminghttp import register_openers


__author__ = 'Pablo Moles'


def reverseImageSearch(filename, verbose):  # Reverse image search on google

    """
    :param filename: String which holds the absolute path to the image
    :param verbose: Boolean that indicates the level of verbosity
    :return: String which contains best label for the given image
    """

    register_openers()

    datagen, headers = multipart_encode({'encoded_image': open(filename, 'r')})
    request = urllib2.Request(URL, datagen, headers)

    print_grey_on_white('Searching on Google ' + filename)

    if verbose:
        print_yellow(requestToString(request))

    urllib2.urlopen(request).read()
    urls = request.redirect_dict.keys()

    for url in urls:
        if isSearchUrl(url):  # Scrap urls which do not result in a search
            headers = {'User-Agent': USERAGENT}
            req = urllib2.Request(url, None, headers)

            if verbose:
                print_yellow(requestToString(req))

            html = urllib2.urlopen(req).read()  # Get and parse results of search
            label = BeautifulSoup(html).findAll('a', {'class': 'qb-b'})[0].string

            return label


def userDefinedLabel(label):
    """
    :param label: String which holds the user-defined label for the a the image, by reference
    """
    label = ''
    while label == '':
        print_blue('User-defined label:')
        label = raw_input()


def main(args):
    g = Graph()
    paths = list()

    directory = getDirectoryFromPath(args.path)

    if args.recursive:
        try:
            paths.extend(findAllImages(directory))
        except Exception:
            print_red(('It was not possible to find any image for the specified path. '
                       'Please try executing with -h and read the argument\'s description.'))
            sys.exit(1)
    else:
        paths.append(directory)

    for path in paths:

        printSeparator(path)

        try:
            img = BNode()

            label = reverseImageSearch(path, args.verbose)

            print_blue('Is it a \"' + label + '\"? (y/n)')
            userInput = getBooleanFromString(raw_input())
            if not userInput:
                userDefinedLabel(label)
        except IndexError:
            label = '*LABEL NOT FOUND*'
            print_red('It was not possible to find any label for the given image. Please provide one.')
            userDefinedLabel(label)
        except IOError:
            label = '*IMAGE NOT FOUND*'
            print_red(('It was not possible to read the image. '
                       'Please try executing with -h and read the argument\'s description.'))
            if not args.recursive:
                sys.exit(1)

        if label != '':
            g.add((img, RDF.type, FOAF.Image))
            g.add((img, FOAF.name, Literal(path)))
            g.add((img, FOAF.depicts, Literal(label)))

    if args.verbose:
        print_yellow('RDF:\n' + g.serialize(format='xml'))

    dumpGraphToDisk(g, args.output, args.verbose)


if __name__ == '__main__':
    printHeader()
    arguments = parseArguments(1)
    main(arguments)
