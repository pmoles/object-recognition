import os
import re
import sys
import argparse

from termcolor import cprint
from constants import SEARCHURL
from findtools.find_files import (find_files, Match)


__author__ = 'Pablo Moles'


def findAllImages(path):
    """
    Recursively find all images in specified path
    :param path: Absolute path to folder
    :return: List of images found recursively
    """
    images = list()
    sh_files_pattern = Match(filetype='f', name=re.compile(r'.*(jpg|jpeg|png|gif)$'))
    found_files = find_files(path=path, match=sh_files_pattern)

    if len(found_files):
        raise IOError

    for found_file in found_files:
        images.append(found_file)

    return images

def dumpGraphToDisk(graph, filename, verbose):
    """
    Dumps graph into a xml
    :param graph: Graf containing RDF
    :param filename: String containing file name
    :param verbose: Level of verbosity
    """
    try:
        filename = getFilename(filename)

        if verbose:
            print_yellow('Filename: ' + filename)

        f = open(getFilename(filename), 'w+')
        f.write(graph.serialize(format='xml'))
        print_green('Information successfully stored in ' + filename + '.')
    except Exception:
        print_red('Cannot write RDF to disk. Please double check permissions.')


def getFilename(filename):
    """
    Selects name for the output file. Defaults to output.rdf
    :param filename: String provided as argument output
    :return: String containing selected filename
    """
    if filename == None:
      filename = 'output.rdf'
    elif '.rdf' not in filename:
        filename += '.rdf'

    return filename


def getBooleanFromString(string):
    """
    Reads input and casts as string
    :param string: Raw user input
    :return: Boolean depending on input
    """
    return string.lower() in ('yes', 'y', 'true')

def getDirectoryFromPath(path):
    """
    Returns working directoy if provided as an argument
    :param string: Specified path
    :return: string: Directory to work on
    """
    if path == ".":
        return os.getcwd()
    return path

def isSearchUrl(url):
    """
    Checks if it's a real search result
    :param url: String containing url to check
    :return: Boolean
    """
    return SEARCHURL in url


def requestToString(req):
    """
    Prints urllib2.Request object
    :param req: urllib2.Request object
    :return: String to pretty print
    """
    return req.get_method(), req.get_full_url(), 'Headers:', [x + ': ' + req.headers[x] for x in req.headers.keys()]


def parseArguments(mode):

    """
    :return: Arguments object, containing all user provided argument information
    """

    if mode == 1:
        desc = 'Will output a best match label for a given image.'
    else:
        desc = 'Query a RDF.'

    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('--verbose', '-V', help='Increase output verbosity.', action='store_true')
    if mode == 1:   # If it's main program
        parser.add_argument('--path', '-p', help='Path to the image or folder to tag.', dest='path')
        parser.add_argument('--recursive', '-R', help='Recursively tags all images encountered. \
                            Remember to provide a path to a folder when using this argument.', action='store_true')
        parser.add_argument('--output', '-o', help='Output filename. Defaults to output.rdf')
    else:   # If it's query program
        parser.add_argument('--input', '-i', help='Input filename. Defaults to output.rdf.')
        group = parser.add_mutually_exclusive_group()
        group.add_argument('--execute', '-x', help='Query to execute.')
        group.add_argument('--console', '-c', help='Console mode is turned on.', action='store_true')

    args = parser.parse_args()

    if mode == 1 and not args.path:
        parser.error('--path/-p needs to be provided.')

    if mode == 2 and not args.console and args.execute is None:
        parser.error('Either --execute/-e or --console/-c need to be provided.')

    if args.verbose:
        print_yellow('Arguments: ' + str(args))

    return args


def printHeader():
    """
    Prints CLI header
    """
    print_grey_on_white('####################################')
    print_grey_on_white('#####    Object Recognition    #####')
    print_grey_on_white('####################################')
    print
    sys.stdout.flush()


def printSeparator(path):
    """
    Prints a separator as long as path
    :param path: String containing image absolute path
    """
    if path is not None:
        print
        print_grey_on_white('-' * (len(path) + 20))


print_white_on_grey = lambda x: cprint(x, 'white', 'on_grey')
print_grey_on_white = lambda x: cprint(x, 'grey', 'on_white')
print_blue = lambda x: cprint('\n' + x, 'blue', attrs=['bold'])
print_yellow = lambda x: cprint('\n[DEBUG] ' + str(x), 'yellow', attrs=['bold'])
print_green = lambda x: cprint('\n[INFO] ' + str(x), 'green', attrs=['bold'])
print_red = lambda x: cprint('\n[ERROR] ' + str(x), 'red', attrs=['bold'])
